import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path:"/",
    redirect:"/home",
  },
  {
    path: "/home",
    name: "home",
    component: ()=>import('../views/Home.vue'),
  },
  {
    path: "/classify",
    name: "classify",
    component: ()=>import('../views/Classify.vue'),
  },{
    path: "/message",
    name: "message",
    component: ()=>import('../views/Message.vue'),
  },{
    path: "/cart",
    name: "cart",
    component: ()=>import('../views/Cart.vue'),
  },{
    path: "/mine",
    name: "mine",
    component: ()=>import('../views/Mine.vue'),
  },
  {
    path: "/reg",
    name: "reg",
    component: ()=>import('../views/Reg.vue'),
    meta : {
      ishide:true,
    }
  },
  {
    path: "/login",
    name: "login",
    component: ()=>import('../views/Login.vue'),
    meta : {
      ishide:true,
    }
  },
];

const router = new VueRouter({
  routes
});

export default router;
