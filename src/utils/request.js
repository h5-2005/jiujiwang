const axios = require('axios'); //引入axios模块

const request = axios.create({
    baseURL: '/',//设置发送请求基础路径（线上接口的ip地址）
    timeout: 5000,//设置最大响应时间
    // headers: {'Authorization': 'token'} 
  });

  //拦截器可监听到发送请求前和响应数据后

  module.exports = request;//request==axios 但是又比axios多了一些基础设置，
